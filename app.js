const express = require("express");
const app = express();
const mongoose = require("mongoose");
const config = require("./app/config/config.json");
const db_dev_config = require("./app/config/develop.json");
var mongodbUrl =
  process.env.PROB_MONGODB ||
  `mongodb://${db_dev_config.DB_URI}:${db_dev_config.DB_PORT}/${
    db_dev_config.DB_NAME
  }`;
mongodbUrl = config.MongoDeploy;
var Users = require("./app/routes/User");
var Login = require("./app/routes/Login");
var Upload = require("./app/routes/Upload");
//Midware request log
app.use((req, res, next) => {
  console.log(Date.now(), req.url);
  next();
});

//Init mongodb connection
mongoose.connect(mongodbUrl, { useNewUrlParser: true }, err => {
  if (err) console.log(err);
  else console.log("db connected");
});
var db = mongoose.connection;
mongoose.Promise = global.Promise;

//Handle error database
db.on("err", err => {
  console.log(err);
});

//Handle route
app.use("/users", Users);
app.use("/login", Login);
app.use("/upload", Upload);
module.exports = app;
