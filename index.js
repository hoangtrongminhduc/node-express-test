const app = require('./app');

//Launch app
var port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log("server is running " + port);
});