var userModel = require("../db/UserModel");
var router = require("express").Router();
const bodyPaser = require("body-parser");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const config = require("../config/config.json");
const CheckToken = require("../middleware/CheckToken").CheckToken;

router.get("/", CheckToken, (req, res) => {
  res.json({
    message: "You are login already, you will be redirect to another page"
  });
  res.end();
});

router.post("/", bodyPaser.json(), (req, res) => {
  let username = req.body.username;
  let plainPassword = req.body.password;
  userModel.findOne({ username: username }).exec((err, rs) => {
    if (err) {
      res.statusCode = 500;
      res.statusMessage = "Internal server error";
    } else {
      if (rs) {
        let user = rs;
        let inputHassPass = crypto
          .createHash("SHA256", user.salt)
          .update(plainPassword + user.saltPassword)
          .digest("hex");
        if (inputHassPass === user.hashPassword) {
          let token = jwt.sign({ username }, config.SECRECT_WORD, {
            expiresIn: "24h"
          });
          res.json({ token });
        } else {
          res.json({ message: "Invalid username or password" });
        }
      } else {
        res.statusCode = 404;
        res.statusMessage = "Not found";
      }
    }
    res.end();
  });
});

module.exports = router;
