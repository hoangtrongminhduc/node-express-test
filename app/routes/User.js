const express = require("express");
var router = express.Router();
const bodyPaser = require("body-parser");
var userModel = require("../db/UserModel");
const crypto = require("crypto");
const config = require("../config/config.json");

//Handle GET /
router.get("/", (req, res) => {
  userModel.find({}).exec((err, rs) => {
    if (err) {
      res.statusCode = 500;
    } else {
      res.json(rs);
    }
    res.end();
  });
});

//Handle POST /
router.post("/", bodyPaser.json(), (req, res) => {
  let userInfo = {};
  console.log(req.body);
  userInfo.username = req.body.username;
  userInfo.firstname = req.body.firstname;
  userInfo.lastname = req.body.lastname;
  userInfo.gender = req.body.gender && req.body.gender === 0 ? false : true;
  userInfo.email = req.body.email;
  userInfo.password = req.body.password;
  userInfo.saltPassword = crypto.randomBytes(16).toString("hex");
  //update password with hash password and salt
  userInfo.hashPassword = crypto
    .createHash("SHA256", config.SECRECT_WORD)
    .update(userInfo.password + userInfo.saltPassword)
    .digest("hex");
  var user = new userModel(userInfo);
  user.save((err, user) => {
    if (err) {
      res.statusCode = 500;
      res.json(err);
    } else {
      let responseData = {};
      responseData.message = "New user created successful";
      responseData.data = JSON.parse(JSON.stringify(user));
      delete responseData.data.password;
      delete responseData.data.saltPassword;
      res.json(responseData);
    }
    res.end();
  });
});

//Handle GET /:id
router.get("/:id", (req, res) => {
  var id = req.params.id;
  userModel.findById(id, { _id: 0, username: 1 }, (err, rs) => {
    if (err) {
      res.statusCode = 500;
    } else {
      if (rs) {
        res.json(rs);
      } else {
        res.statusCode = 404;
        res.json({ message: "Not found" });
      }
    }
    res.end();
  });
});

//Handle PUT /:id
router.put("/:id", bodyPaser.json(), (req, res) => {
  var id = req.params.id;
  var user = {};
  console.log(user, req.body);
  req.body.username ? (user.username = req.body.username) : null;
  req.body.firstname ? (user.firstname = req.body.firstname) : null;
  req.body.lastname ? (user.lastname = req.body.lastname) : null;
  req.body.gender && (req.body.gender === 0 || req.body.gender === 1)
    ? (user.gender = req.body.gender)
    : null;
  req.body.email ? (user.email = req.body.email) : null;
  userModel.findByIdAndUpdate(id, user, (err, rs) => {
    if (err) {
      res.statusCode = 500;
      res.json(err);
    } else {
      res.json(rs);
      console.log(res);
    }
    res.end();
  });
});

//Handle DELETE /:id
router.delete("/:id(\d+)", (req, res) => {
  var id = req.params.id;
  userModel.findByIdAndDelete(id, (err, rs) => {
    if (err) {
      res.statusCode = 500;
      res.json(err);
    } else {
      res.json(rs);
    }
    res.end();
  });
});


module.exports = router;
