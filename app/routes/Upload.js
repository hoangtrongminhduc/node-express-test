const path = require("path");
var router = require("express").Router();
const multer = require("multer");
const cloudinary = require("cloudinary").v2;
const uploader = multer({ dest: path.join(__dirname, "../files") });
const cldAccess = require("../config/config.json").cloudinary_access;
//Midware
cloudinary.config(cldAccess);
//Route
router.post("/", uploader.single("avatar"), (req, res) => {
  cloudinary.uploader.upload(req.file.path, (err, rs) => {
    if (err) {
      console.log(err);
      res.json(err);
    } else {
      if (rs) {
        console.log(rs);
        res.json(rs);
      } else {
        console.log("Nothing");
      }
    }
    res.end();
  });
});

module.exports = router;
