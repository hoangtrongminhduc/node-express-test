const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let userSchema = new Schema(
  {
    username: String,
    firstname: String,
    lastname: String,
    gender: Boolean,
    email: String,
    hashPassword: {
      type: String,
      required: true
    },
    saltPassword: {
      type: String,
      required: true
    }
  },
  { versionKey: false }
);

module.exports = mongoose.model("User", userSchema);
