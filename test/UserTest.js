var chai = require("chai");
var chaihttp = require("chai-http");
var should = chai.should();
var server = require("../app");
chai.use(chaihttp);

describe("User", () => {
  before(done => {
    done();
  });
  describe("GET /users", () => {
    it("should get all users", done => {
      chai
        .request(server)
        .get("/users")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a("array");
          done();
        });
    });
  });
  describe("GET /users/:id", () => {
    let id = "5d0b4cf2b083ac1ce1b7991e";
    it("should get an user", done => {
      chai
        .request(server)
        .get("/users/" + id)
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            res.should.have.status(200);
            res.body.should.be.a("object");
            done();
          }
        });
    });
  });
  describe("POST /users", () => {
    it("should create an user", done => {
      chai
        .request(server)
        .post("/users")
        .send({
          username: "abc",
          lastname: "",
          firstname: "",
          email: "a@gmal.com",
          password: "123"
        })
        .end((err, res) => {
          if (err) {
            done(err);
          } else {
            res.should.have.status(200);
            res.body.should.be.a("object");
            done();
          }
        });
    });
  });
});
